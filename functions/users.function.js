const functions = require('firebase-functions');
const admin = require('firebase-admin');

const cors = require('cors')({origin: true});

try { admin.initializeApp(functions.config().firebase) } catch (e) {}

const usersRef = admin.firestore().collection('users')

exports.createUserAccount = functions.auth.user().onCreate( event => {
    const uid = event.data.uid
    const dateCreated = new Date()

    const newUserRef = usersRef.doc(uid)
    return newUserRef.set({
        dateCreated: dateCreated
    })
})

exports.deleteUserAccount = functions.auth.user().onDelete( event => {
    const uid = event.data.uid
    console.log(uid)
    return usersRef.doc(uid).delete()
})
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const glob = require('glob') // npm i -S glob
const files = glob.sync('./**/*.function.js', { cwd: __dirname, ignore: './node_modules/**' })

files.forEach(file => {
  const functionModule = require(file)
  const functionNames = Object.keys(functionModule)

  functionNames.forEach(functionName => {
    if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === functionName) {
      exports[functionName] = functionModule[functionName]
    }
  })
})
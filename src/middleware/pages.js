export default function(context) {
  // go tell the store to update the page
  this.$store.updatePage = context.route.name
}

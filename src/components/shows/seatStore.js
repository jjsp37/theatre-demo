export const seats = {
  rows: {
    a: [
      {
        id: "a1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a13",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "a14",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    b: [
      {
        id: "b1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: " b3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: " b6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b13",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "b14",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    c: [
      {
        id: "c1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c13",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "c14",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    d: [
      {
        id: "d1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "d13",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    e: [
      {
        id: "e1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "e12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    f: [
      {
        id: "f1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "f12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    g: [
      {
        id: "g1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "g12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    h: [
      {
        id: "h1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "h12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    i: [
      {
        id: "i1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "i12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    j: [
      {
        id: "j1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "j12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    k: [
      {
        id: "k1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "k2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "k3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "k4",
        status: "available",
        type: "obstruction",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "k5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "k6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "k7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      },
      {
        id: "k8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "floor"
      }
    ],
    aa: [
      {
        id: "aa1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa13",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "aa14",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      }
    ],
    bb: [
      {
        id: "bb1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb13",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "bb14",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      }
    ],
    cc: [
      {
        id: "cc1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc12",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc13",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "cc14",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      }
    ],
    dd: [
      {
        id: "dd1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "dd11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      }
    ],
    ee: [
      {
        id: "ee1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ee11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      }
    ],
    ff: [
      {
        id: "ff1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "ff11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      }
    ],
    gg: [
      {
        id: "gg1",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg2",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg3",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg4",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg5",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg6",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg7",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg8",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg9",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg10",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg11",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "blank1",
        status: "available",
        type: "empty",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "blank2",
        status: "available",
        type: "empty",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "blank3",
        status: "available",
        type: "empty",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg15",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg16",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      },
      {
        id: "gg17",
        status: "available",
        timer: 0,
        dateSelected: 0,
        level: "mezzanine"
      }
    ]
  }
};
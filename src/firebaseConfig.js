import * as firebase from 'firebase'
import 'firebase/firestore'

const config = {
	apiKey: "AIzaSyD_eaobq7AMZx2AglQSNzCRA-X8CDeTQJQ",
    authDomain: "theatre-969b8.firebaseapp.com",
    databaseURL: "https://theatre-969b8.firebaseio.com",
    projectId: "theatre-969b8",
    storageBucket: "theatre-969b8.appspot.com"
}

const firebaseApp = firebase.initializeApp(config)

export const db = firebase.firestore()
export const auth = firebase.auth()
export const dbShows = db.collection('shows')
export const dbUsers = db.collection('users')
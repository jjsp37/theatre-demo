// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import VueStash from 'vue-stash'
import router from './router'

Vue.use(Buefy)
Vue.use(VueStash)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {
    store: {
      pageTitle: 'You are awesome',
      updatePage: ''
    }
  },  
  router,
  components: { App },
  template: '<App/>'
})

let userStorage = JSON.parse(localStorage.getItem('user'));
import { seatLogic } from "./seatLogic";
import { seats } from "../components/shows/seatStore";

export const hydrate = {
    methods: {
        
       showStorage(show) {

            if (localStorage.getItem(show)){
                // Load Show
                this.show = JSON.parse(localStorage.getItem(show))
            }        

            if (userStorage) {
                if (userStorage[`selected-${show}`]) {
                    // Load Selected Seats
                    for (let seat of userStorage[`selected-${show}`]) {
                        if (!this.selected.find( item => item.id === seat.id)) {
                            //alert('test')
                            seat.dateSelected = new Date(seat.dateSelected) 
                            let seatWithCountdown = this.seatCountdown(seat)
                            this.selected.push(seatWithCountdown)
                        }
                    }
                }                
            }
       },
        cartStorage() {
            // Load Selected Seats
            for (let storage of Object.keys(userStorage)) {
                if (storage.startsWith('selected-')) {
                    const title = storage.substring(9, storage.length)
                    if (!this.showIDS.find( item => item === title)) {
                        this.showIDS.push(title)
                    }
                    userStorage[storage].forEach(seat => {
                        if (!this.selected.find( item => item.id === seat.id)) {
                            seat.showID = title 
                            seat.dateSelected = new Date(seat.dateSelected) 
                            let seatWithCountdown = this.seatCountdown(seat)
                            this.selected.push(seatWithCountdown)
                        }                        
                    })
                }            
            }
        }        
    }
}
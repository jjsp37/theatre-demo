import { dbShows, auth, dbUsers } from "../firebaseConfig";

export const seatLogic = {
    methods: {
        seatAvailable(seat) {
            if (seat.status === "available") {
              return true;
            } else if (seat.status === 'pending' && this.selected.find(item => item.id === seat.id)) {
              return true;
            } else {
              return false;
            }
          },
        seatCountdown(seat) {        
            // Get the current time
            let currentTime = new Date().getTime()

            // Get the time the seat was selected
            let dateSelected = seat.dateSelected.getTime()

            // Compare them both
            let diff = Math.abs(dateSelected - currentTime) 

            // Default duration (set to 30 seconds for demo)
            let duration = 30 * 1000 * 60 // -- convert to minutes
            
            // Compare both times, and set timer to 0 if it's over the limit otherwise set to defailt
            if (diff > duration) {
            seat.timer = 0
            seat['countdown' + seat.id] = null
            } else {                                                               // Or a random time between 1 and 30 seconds
            seat.timer ? seat.timer = (duration - diff) : seat.timer = duration // Math.floor((Math.random() * 30) + 1) * 1000
            }

            // Start the countdown
            seat['countdown' + seat.id] = setInterval(() => {

            //Take 1 second off the time, every second.
            seat.timer -= 1000   
            
            //When timer is finished, clear the interval and un-select seat.
            if ( seat.timer <= 0 ) {
                clearInterval(seat['countdown' + seat.id])

                // selectSeat does both selecting and un-selecting
                this.selectSeat(seat, seat.id)
            }
            // console.log('Countdown timer for ' + seat.id + ' ' + seat.timer)
            
            // If we don't return the seat then it won't have .timer or .countdown so we can't work with it in the future (aka page reload)
            return seat
            }, 1000)

            // And return it to the original call
            return seat
        },
        selectSeat(seat, index) {
            
            const show = this.showID ? this.showID : seat.showID

            // record the date / time it was selected
            seat.dateSelected = new Date()

            // Extract what level the seats located on (for API query)
            const level = seat.level

            // Extract the number from the ID e.g cc13 will return 13
            const number = index.match(/\d+$/)[0] -1

            // Extract the letter e.g cc13 will return cc
            const letter = index.replace(/[0-9]/g, '')

            // Check that the seat is not already sold or selected
            if (
            !this.selected.find(item => item.id === seat.id) &&
            this.seatAvailable(seat) &&
            seat.type == undefined &&
            seat.type == null
            ) {
            // Start timer, which will return the seat with .timer and .countdown 
            let seatWithCountdown = this.seatCountdown(seat)       

            // Add the returned seat to selected array
            this.selected.push(seatWithCountdown)


            // Send it to the users database 
            dbUsers
                .doc(this.userDetails.id)
                .update(`selected.${show}`, this.selected)
                .catch(err => {
                console.log(err);
                });

            // Check the current status of the seats
            dbShows
                .doc(show)
                .get()
                .then( async snapshot => {            
                // Get the current status
                let data = snapshot.data()  

                const row = data.tickets[letter]


                // Update Seat To Pending -- Whole row has to come with it (firebase limitation)
                row[number].status = 'pending'

                let rowUpdate = {}

                // Update the whole row
                rowUpdate[`tickets.${letter}`] = row

                await dbShows.doc(show).update(rowUpdate)

                })
                .catch(err => console.log(err));
            } else if (
            this.seatAvailable(seat) &&
            seat.type == undefined &&
            seat.type == null
            ) {        

            let selectedSeat = this.selected.find(el => el.id === seat.id)

            // Remove the seat from current selected array
            this.selected.splice(this.selected.findIndex(el => el.id === seat.id), 1)

            // Remove the countdown timer
            clearInterval(selectedSeat['countdown' + index])
            selectedSeat.timer = 0
            
            // Update the user's DB profile to remove the seat from their selected/pending
            dbUsers
                .doc(this.userDetails.id)
                .update(`selected.${show}`, this.selected)
                .catch(err => {
                console.log(err);
                });

                // Set the seat back to available
                dbShows
                .doc(show)
                .get()
                .then( async snapshot => {
                let data = snapshot.data()  
                const row = data.tickets[letter]

                //Update Seat To Pending -- Whole row has to come with it (firebase limitation)
                row[number].status = 'available'

                let rowUpdate = {}
                rowUpdate[`tickets.${letter}`] = row
                
                await dbShows.doc(show).update(rowUpdate)   

                })
                .catch(err => console.log(err));
            }
        },        
    }
}
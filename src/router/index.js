import Vue from 'vue'
import Router from 'vue-router'
import TicketGrid from '@/components/tickets/TicketGrid.vue'
import Cart from '@/components/tickets/Cart.vue'
import NewShow from '@/components/shows/NewShow.vue'
import Shows from '@/components/shows/Shows.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkExactActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      name: 'New Show',
      component: NewShow
    },
    {
      path: '/shows',
      name: 'Shows',
      component: Shows    
    },
    {
      path: '/show/:id',
      name: 'Show',
      component: TicketGrid    
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart    
    }
  ]
})

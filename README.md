# theatre

> Ticket System inspired by the Milton Ulladulla website (created by Ulladulla Web Design) using vuejs and Googles firebase
> Live URL https://theatre-969b8.firebaseapp.com/#/show/the-lachy-doley-group
> (needs to be that url and not the root)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
